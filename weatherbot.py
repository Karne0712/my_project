import requests
import telebot

url = 'http://api.openweathermap.org/data/2.5/weather'

api_weather = ('input key api ')
api_telegram =('input key api')

bot = telebot.TeleBot(api_telegram)


@bot.message_handler(commands=['start'])
def welcome(message):
	sti = open('welcome.png', 'rb')
	bot.send_sticker(message.chat.id, sti)
	bot.send_message(message.chat.id, 'Բարև գժուկ, ' + str(message.from_user.username) + ',' + '\n' +
	 'Գրիր ինձ ցանկացած բնակավայրի անունը ես քեզ կտեղեկացնեմ թե ինչ եղանակ է այնտեղ!')


@bot.message_handler(content_types=['text'])
def weather_send(message):
	s_city = message.text
	try:
		params = {'APPID': api_weather, 'q': s_city, 'units': 'metric'}
		result = requests.get(url, params=params)
		weather = result.json()

		bot.send_message(message.chat.id, "Բնակավայր " + str(weather["name"]) + " ջերմաստիճանը " + str(float(weather["main"]['temp'])) + "\n" +
				"Առավելագույն րերմաստիճանը " + str(float(weather['main']['temp_max'])) + "\n" +
				"Նվազագույն րերմաստիճանը " + str(float(weather['main']['temp_min'])) + "\n" +
				"Քամու արագությունը " + str(float(weather['wind']['speed'])) + "\n" +
				"Մնթնոլորտային ճնշումը " + str(float(weather['main']['pressure'])) + "\n" +
				"Խոնավությունը " + str(float(weather['main']['humidity'])) + "\n" +
				"Տեսանելիությունը " + str(weather['visibility']) + "\n" +
				"-_- " + str(weather['weather'][0]["description"]) + "\n")

		if weather["main"]['temp'] < 10:
			bot.send_message(message.chat.id, "Ցուրտ է ինչքան շոր ունես տանը հագի!")
		elif weather["main"]['temp'] < 20:
			bot.send_message(message.chat.id, "Նեց ոչնչոտ եղանակ է")
		elif weather["main"]['temp'] > 38:
			bot.send_message(message.chat.id, "Պետքա քեզ էս շոգին տանից դուրս գաս!")
		else:
			bot.send_message(message.chat.id, "Թույն եղանակ է!")

	except:
		bot.send_message(message.chat.id, "Բնակավայր " + s_city + " չեմ գտել!!! ախպերական մի հատ ուշադիր եղի ճիշտ ես գրել")


if __name__ == '__main__':
	bot.polling(none_stop=True)